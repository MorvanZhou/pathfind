import time

import pathfind

m = [[2, 1, 2, 1, 2, 1, 2, 1, 1, 1, 1, 2, 2, 1, 2], [1, 2, 1, 1, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1, 2],
     [1, 1, 1, 2, 2, 1, 2, 2, 2, 1, 2, 2, 1, 2, 2], [2, 1, 1, 2, 2, 1, 1, 2, 2, 2, 2, 1, 2, 1, 1],
     [1, 2, 1, 2, 2, 2, 1, 1, 2, 1, 2, 1, 2, 1, 1], [2, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2],
     [2, 1, 2, 2, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1], [1, 2, 2, 2, 1, 1, 1, 2, 1, 2, 2, 2, 2, 1, 2],
     [2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1], [1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1],
     [1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 1], [1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1],
     [2, 2, 2, 1, 2, 1, 2, 2, 2, 1, 1, 1, 2, 1, 1], [1, 1, 1, 2, 2, 2, 1, 2, 2, 1, 1, 2, 2, 2, 1],
     [1, 1, 2, 2, 2, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2]]

g = pathfind.transform.matrix2graph(m)
for method in [
    "a*",
    "bfs",
    "greedy",
    "dijkstra",
    "dfs",
    "d*lite",
    "jps"
]:
    ts = []
    for i in range(20):
        t0 = time.time()
        for j in range(15):
            pathfind.find(g, "7,7", "14,14", method=method)
        t1 = time.time()
        ts.append(t1 - t0)
    print(f"{method}: max={max(ts):.4f}, mean={sum(ts) / len(ts):.4f}")
